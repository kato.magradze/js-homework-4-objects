let students = [];

students[0] = {
    name: "Jean",
    lastName: "Reno",
    age: 26,
    scores: {
        javascript: 62,
        react: 57,
        python: 88,
        java: 90
    },
    individualGPA: {
        javascript: 1,
        react: 0.5,
        python: 3,
        java: 3
    }
}

students[1] = {
    name: "Claude",
    lastName: "Monet",
    age: 19,
    scores: {
        javascript: 77,
        react: 52,
        python: 92,
        java: 67
    },
    individualGPA: {
        javascript: 2,
        react: 0.5,
        python: 4,
        java: 1
    }
}

students[2] = {
    name: "Van",
    lastName: "Gogh",
    age: 21,
    scores: {
        javascript: 51,
        react: 98,
        python: 65,
        java: 70
    },
    individualGPA: {
        javascript: 0.5,
        react: 4,
        python: 1,
        java: 1
    }
}

students[3] = {
    name: "Dam",
    lastName: "Square",
    age: 36,
    scores: {
        javascript: 82,
        react: 53,
        python: 80,
        java: 65
    },
    individualGPA: {
        javascript: 3,
        react: 0.5,
        python: 2,
        java: 1
    }
}

//console.log(students);

//Total Points for each student

students[0].stats = {};
students[0].stats.totalScore = students[0].scores.javascript + students[0].scores.react + students[0].scores.python + students[0].scores.java;
console.log(`Jean Reno Total Points ${students[0].stats.totalScore}`);

students[1].stats = {};
students[1].stats.totalScore = students[1].scores.javascript + students[1].scores.react + students[1].scores.python + students[1].scores.java;
console.log(`Claude Monet Total Points ${students[1].stats.totalScore}`);

students[2].stats = {};
students[2].stats.totalScore = students[2].scores.javascript + students[2].scores.react + students[2].scores.python + students[2].scores.java;
console.log(`Van Gogh Total Points ${students[2].stats.totalScore}`);

students[3].stats = {};
students[3].stats.totalScore = students[3].scores.javascript + students[3].scores.react + students[3].scores.python + students[3].scores.java;
console.log(`Dam Square Total Points ${students[3].stats.totalScore}`);
console.log("\n");

//Total Average for each student
const numOfSubjects = 4;

students[0].stats.AvgScore = students[0].stats.totalScore / numOfSubjects;
console.log(`Jean Reno Average Points: ${students[0].stats.AvgScore}`);

students[1].stats.AvgScore = students[1].stats.totalScore / numOfSubjects;
console.log(`Claude Monet Average Points: ${students[1].stats.AvgScore }`);

students[2].stats.AvgScore = students[2].stats.totalScore / numOfSubjects;
console.log(`Van Gogh Average Points: ${students[2].stats.AvgScore}`);

students[3].stats.AvgScore = students[3].stats.totalScore / numOfSubjects;
console.log(`Dam Square Average Points: ${students[3].stats.AvgScore}`);
console.log("\n");

//GPA
const credits = {
    javascript: 4,
    react: 7,
    python: 6,
    java: 3
}

credits.total = credits.javascript + credits.react + credits.python + credits.java;

//GPA Student 1

students[0].individualGPA.javascriptAverage = students[0].individualGPA.javascript * credits.javascript;
students[0].individualGPA.reactAverage = students[0].individualGPA.react * credits.react;
students[0].individualGPA.pythonAverage = students[0].individualGPA.python * credits.python;
students[0].individualGPA.javaAverage = students[0].individualGPA.java * credits.java;

students[0].stats.GPA = (students[0].individualGPA.javascriptAverage + students[0].individualGPA.reactAverage + students[0].individualGPA.pythonAverage + students[0].individualGPA.javaAverage) / credits.total;
console.log(`Jean Reno GPA: ${students[0].stats.GPA}`);

//GPA Student 2

students[1].individualGPA.javascriptAverage = students[1].individualGPA.javascript * credits.javascript;
students[1].individualGPA.reactAverage = students[1].individualGPA.react * credits.react;
students[1].individualGPA.pythonAverage = students[1].individualGPA.python * credits.python;
students[1].individualGPA.javaAverage = students[1].individualGPA.java * credits.java;

students[1].stats.GPA = (students[1].individualGPA.javascriptAverage + students[1].individualGPA.reactAverage + students[1].individualGPA.pythonAverage + students[1].individualGPA.javaAverage) / credits.total;
console.log(`Claude Monet GPA: ${students[1].stats.GPA}`);

//GPA Student 3
students[2].individualGPA.javascriptAverage = students[2].individualGPA.javascript * credits.javascript;
students[2].individualGPA.reactAverage = students[2].individualGPA.react * credits.react;
students[2].individualGPA.pythonAverage = students[2].individualGPA.python * credits.python;
students[2].individualGPA.javaAverage = students[2].individualGPA.java * credits.java;

students[2].stats.GPA = (students[2].individualGPA.javascriptAverage + students[2].individualGPA.reactAverage + students[2].individualGPA.pythonAverage + students[2].individualGPA.javaAverage) / credits.total;
console.log(`Van Gogh GPA: ${students[2].stats.GPA}`);

//GPA Student 4
students[3].individualGPA.javascriptAverage = students[3].individualGPA.javascript * credits.javascript;
students[3].individualGPA.reactAverage = students[3].individualGPA.react * credits.react;
students[3].individualGPA.pythonAverage = students[3].individualGPA.python * credits.python;
students[3].individualGPA.javaAverage = students[3].individualGPA.java * credits.java;

students[3].stats.GPA = (students[3].individualGPA.javascriptAverage + students[3].individualGPA.reactAverage + students[3].individualGPA.pythonAverage + students[3].individualGPA.javaAverage) / credits.total;
console.log(`Dam Square GPA: ${students[3].stats.GPA}`);
console.log("\n");

//Students Total Average
const numOfStudents = students.length;

const studentTotals = {};

studentTotals.totalAverage = (students[0].stats.AvgScore + students[1].stats.AvgScore + students[2].stats.AvgScore + students[3].stats.AvgScore) / numOfStudents;
console.log(`Average Total Points for students: ${studentTotals.totalAverage}`);
console.log("\n");

//Students Status
students[0].stats.status = (students[0].stats.AvgScore >= studentTotals.totalAverage) ? "Red Diploma" : "Enemy of the People";
console.log(`Status for Jean Reno: ${students[0].stats.status}`);

students[1].stats.status = (students[1].stats.AvgScore >= studentTotals.totalAverage) ? "Red Diploma" : "Enemy of the People";
console.log(`Status for Claude Monet: ${students[1].stats.status}`);

students[2].stats.status = (students[2].stats.AvgScore >= studentTotals.totalAverage) ? "Red Diploma" : "Enemy of the People";
console.log(`Status for Van Gogh: ${students[2].stats.status}`);

students[3].stats.status = (students[3].stats.AvgScore >= studentTotals.totalAverage) ? "Red Diploma" : "Enemy of the People";
console.log(`Status for Dam Square: ${students[3].stats.status}`);
console.log("\n");

//Highest GPA

let maxGPA = 0;
let bestByGPA = 0;

if(students[0].stats.GPA > maxGPA) {
    maxGPA = students[0].stats.GPA;
    bestByGPA = 0;
}
if(students[1].stats.GPA > maxGPA) {
    maxGPA = students[1].stats.GPA;
    bestByGPA = 1;
}
if(students[2].stats.GPA > maxGPA) {
    maxGPA = students[2].stats.GPA;
    bestByGPA = 2;
}
if(students[3].stats.GPA > maxGPA) {
    maxGPA = students[3].stats.GPA;
    bestByGPA = 3;
}

console.log(`Student with highest GPA: ${students[bestByGPA].name} ${students[bestByGPA].lastName}`);
console.log("\n");

//Best Student with Average Points (21+)

let bestByAvgPoints21 = 0;
let maxValue = 0;
let over21 = false;

if((students[0].age >= 21) && (students[0].stats.AvgScore > maxValue)) {
    maxValue = students[0].stats.AvgScore;
    bestByAvgPoints21 = 0;
    over21 = true;
}
if((students[1].age >= 21) && (students[1].stats.AvgScore  > maxValue)) {
    maxValue = students[1].stats.AvgScore;
    bestByAvgPoints21 = 1;
    over21 = true;
}
if((students[2].age >= 21) && (students[2].stats.AvgScore  > maxValue)) {
    maxValue = students[2].stats.AvgScore;
    bestByAvgPoints21 = 2;
    over21 = true;
}
if((students[3].age >= 21) && (students[3].stats.AvgScore  > maxValue)) {
    maxValue = students[3].stats.AvgScore;
    bestByAvgPoints21 = 3;
    over21 = true;
}
// if (!over21) {
//     bestByAvgPoints21 = 'No one is 21+';
// }

console.log(`Student with highest average points (21+): ${students[bestByAvgPoints21].name} ${students[bestByAvgPoints21].lastName}`);
console.log("\n");

//Best Student in Front-end Subjects with Average Points
const numOfFrontSubjects = 2;

students[0].stats.frontAverage = (students[0].scores.javascript + students[0].scores.react) / numOfFrontSubjects;
students[1].stats.frontAverage = (students[1].scores.javascript + students[1].scores.react) / numOfFrontSubjects;
students[2].stats.frontAverage = (students[2].scores.javascript + students[2].scores.react) / numOfFrontSubjects;
students[3].stats.frontAverage = (students[3].scores.javascript + students[3].scores.react) / numOfFrontSubjects;

let maxFront = 0;
let bestInFront = 0;

if (students[0].stats.frontAverage > maxFront) {
    maxFront = students[0].stats.frontAverage;
    bestInFront = 0;
}
if (students[1].stats.frontAverage > maxFront) {
    maxFront = students[1].stats.frontAverage;
    bestInFront = 1;
}
if (students[2].stats.frontAverage > maxFront) {
    maxFront = students[2].stats.frontAverage;
    bestInFront = 2;
}
if (students[3].stats.frontAverage > maxFront) {
    maxFront = students[3].stats.frontAverage;
    bestInFront = 3;
}

console.log(`Student with highest average points in Front-end subjects: ${students[bestInFront].name} ${students[bestInFront].lastName}`);